package com.ranginkaman.mvvm.viewmodel

import androidx.lifecycle.ViewModel
import com.ranginkaman.mvvm.data.Quote
import com.ranginkaman.mvvm.data.QuoteRepository

class QuotesViewModel(private val qouteRepository: QuoteRepository): ViewModel() {

    fun getQoutes() = qouteRepository.getQuote()

    fun addQuote(quote: Quote) = qouteRepository.addQuote(quote)
}