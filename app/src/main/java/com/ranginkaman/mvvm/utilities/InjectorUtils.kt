package com.ranginkaman.mvvm.utilities

import com.ranginkaman.mvvm.data.FakeDatabase
import com.ranginkaman.mvvm.data.QuoteRepository
import com.ranginkaman.mvvm.viewmodel.QuotesViewModelFactory

object InjectorUtils {

    fun provideQuotesViewModelFactory(): QuotesViewModelFactory {
        val quoteRepository = QuoteRepository.getInstance(FakeDatabase.getInstance().quoteDao)
        return QuotesViewModelFactory(quoteRepository)
    }
}